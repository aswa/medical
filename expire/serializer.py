from rest_framework import serializers

from .models import Expire


class ExpireSerializer(serializers.ModelSerializer):
    class Meta:
        model = Expire
        fields = '__all__'
