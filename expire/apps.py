from django.apps import AppConfig


class ExpireConfig(AppConfig):
    name = 'expire'
