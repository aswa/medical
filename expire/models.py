from django.db import models

# Create your models here.


from django.db import models

# Create your models here.
from django.db.models import CASCADE

from product.models import Product


class Expire(models.Model):
    medicine_name = models.ForeignKey(Product, on_delete=CASCADE)
    expire_date = models.DateField()
    batch_no = models.CharField(max_length=20)
    brand = models.CharField(max_length=20)
    return_count = models.IntegerField()

    def __str__(self):
        return self.brand





