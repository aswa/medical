from django.shortcuts import render

# Create your views here.

from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from rest_framework.response import Response

from product.models import Product
from .models import Expire
from .serializer import ExpireSerializer


class ExpireViewSet(viewsets.ModelViewSet):
    queryset = Expire.objects.all()
    serializer_class = ExpireSerializer


    def create(self, request, *args, **kwargs):
        prd = Product.objects.get(medicine_name = request.data['name'])
        brand = prd.brand
        expire_date = prd.expire_date
        batch_no = prd.batch_no
        expire = Expire.objects.create(medicine_name = prd,brand = brand,expire_date = expire_date,batch_no =batch_no )
        return Response(self.serializer_class(expire).data)






