from django.db import models

# Create your models here.
from django.db.models import CASCADE

from product.models import Product


class Stock(models.Model):
    medicine_name = models.ForeignKey(Product,on_delete=CASCADE)
    batch_no = models.CharField(max_length=20)
    expire_date = models.DateField()
    balance_count = models.IntegerField(null= True,blank=True)
    brand = models.CharField(max_length=30)
    sale = models.IntegerField(null=True,blank=True)


    def __str__(self):
        return self.brand



