from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from product.models import Product
from stock.models import Stock
from stock.serializer import StockSerializer


class StockViewSet(viewsets.ModelViewSet):
    queryset = Stock.objects.all()
    serializer_class = StockSerializer

    def create(self, request, *args, **kwargs):
        name = request.data['name']
        pr = Product.objects.get(medicine_name=name)
        expire_date = pr.expire_date
        brand = pr.brand
        stock = Stock.objects.create(medicine_name=pr, expire_date=expire_date,brand=brand)
        return Response(self.serializer_class(stock).data)

    # @action(methods=['get'], detail=False)
    # def search(self, request):
    #     pass


