from django.shortcuts import render

# Create your views here.
from django.views import View
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from product.models import Product
from product.serializer import ProductSerializer
from stock import views


class ProductViewsSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    @action(methods =['post'],detail=False)
    def purchasee(self,request):
        name = request.data['name']
        pr = Product.objects.get(medicine_name =name)
        num = request.data['count']
        pr.purchase(num)
        return Response("done")
