from pickle import FALSE

from django.db import models

# Create your models here.


class Product(models.Model):
    medicine_name = models.CharField(max_length=30)
    batch_no = models.CharField(max_length=30)
    expire_date = models.DateField()
    brand = models.CharField(max_length=30)
    total_no_medicine = models.IntegerField()
    sale = models.IntegerField(null=True,blank=True)
    balance_count = models.IntegerField(null=True,blank=True)


    def __str__(self):
        return self.medicine_name


    def purchase(self, sale):
        self.balance_count = self.total_no_medicine
        self.balance_count -= int(sale)
        self.save()