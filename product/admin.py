from django.contrib import admin

# Register your models here.
from product.models import Product
from stock.models import Stock

admin.site.register(Product)