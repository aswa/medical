"""medical_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from billing.views import BillingViewSet
from employee.views import EmployeeViewSet
from expire.views import ExpireViewSet
from product.views import ProductViewsSet
from stock.views import  StockViewSet

router = routers.SimpleRouter()
router.register('product', ProductViewsSet, basename='pr')
router.register('billing', BillingViewSet, basename='bi')
router.register('stock', StockViewSet, basename='st')
router.register('expire', ExpireViewSet, basename='exp')
router.register('employee', EmployeeViewSet, basename='emp')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls))


]
