from django.db import models


# Create your models here.

class Employee(models.Model):
    employee_name = models.CharField(max_length=30)
    leave = models.IntegerField()
    salary = models.IntegerField()
    salary_cut = models.IntegerField()
    total_salary = models.IntegerField()

    def __str__(self):
        return self.employee_name
