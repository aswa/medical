from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets

from employee.models import Employee
from employee.serializer import EmployeeSerializer


class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
