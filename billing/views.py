from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets, response
from rest_framework.decorators import action
from rest_framework.response import Response

from billing.models import Billing
from billing.serializer import BillingSerializer
from product.models import Product


class BillingViewSet(viewsets.ModelViewSet):
    queryset = Billing.objects.all()
    serializer_class = BillingSerializer


    @action(methods=['post'],detail=False)
    def billing(self,request):
        name = request.data['name']
        mrp = request.data['mrp']
        medicine_count = request.data['medicine_count']
        bil = Billing.objects.get(medicine_name=name,mrp=mrp,medicine_count=medicine_count)
        bil.totals(mrp,medicine_count)
        return Response('done')
