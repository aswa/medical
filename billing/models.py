from django.db import models

# Create your models here.
from django.db.models import CASCADE

from product.models import Product


class Billing(models.Model):
    medicine_name = models.ForeignKey(Product,on_delete=CASCADE)
    medicine_count = models.IntegerField(null=True,blank=True)
    mrp = models.IntegerField(null=True,blank=True)
    total = models.IntegerField(null=True,blank=True)

    def __str__(self):
        return str(self.mrp)

    def totals(self, mrp,medicine_count):
        self.total = int(medicine_count) * int(mrp)
        self.save()
