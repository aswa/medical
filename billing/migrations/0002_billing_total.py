# Generated by Django 3.0.3 on 2020-02-11 09:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='billing',
            name='total',
            field=models.IntegerField(default=10),
        ),
    ]
